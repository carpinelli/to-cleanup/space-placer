#!/usr/bin/env python3

"""Place a space between appropriate text chunks in file names."""
"""Add a space between 'Harvest' and the date in Steele harvest
spreadsheets."""

from pathlib import Path
import re

import pymsgbox


def main() -> int:
    # Build re for harvest and date
    # update to look for harvest!\s && !endswith(harvest)
    harvest_pattern = re.compile(r"(Harvest)(\d{1,2}-\d{1,2})(-\d{1,4})?")
    # Get working directory
    prompt_msg = "Copy the path to the directory containing harvest files here:"
    harvest_directory = Path(pymsgbox.prompt(prompt_msg)).resolve()
    # List .xlsx files
    xlsx_files = list()
    for file_ in harvest_directory.iterdir():
        if file_.suffix.lower() == ".xlsx":
            xlsx_files.append(file_)
    # Search file names for re
    if len(xlsx_files) == 0:
        pymsgbox.alert("No files to rename! Press 'Enter' to exit...")
        return -1  # No files to rename
    for file_ in xlsx_files:
        harvest_match = harvest_pattern.search(file_.name)
        if harvest_match is None:
            continue  # No need to add a space
        # Rename to add a space between "Harvest".lower() and date
        new_name = harvest_directory / Path(f"Harvest {harvest_match.group(2)}.xlsx")
        file_.rename(new_name)

    return 0


if __name__ == "__main__":
    main()
